const app = new Vue({
    el: "#order-section",
    delimiters: ["{$", "$}"],
    data: {
        choosed_products: [],
    },
    computed: {
        gross_value() {
            let arr = this.choosed_products.map(e => e.price * e.count);
            return arr.reduce((a, b) => {
                return a + b;
            }, 0);
        },
        discount() {
            let arr = this.choosed_products.map(e => e.count * e.price * (e.discount / 100));
            return arr.reduce((a, b) => {
                return a + b;
            }, 0);
        },
        subtotal() {
            return this.gross_value - this.discount;
        },
        iva() {
            let arr = this.choosed_products.map(e => e.iva > 0 ? e.count * (e.price - (e.price * e.discount / 100)) * e.iva : 0);
            return arr.reduce((a, b) => {
                return a + b;
            }, 0);
        },
        full_iva() {
            let arr = this.choosed_products.map(e => e.count * e.price * e.iva);
            return arr.reduce((a, b) => {
                return a + b;
            }, 0);
        },
        total() {
            return this.subtotal + this.iva;
        },
        
    }, // end of compute
    methods: {
        removeProduct(index) {
            this.choosed_products.splice(index, 1);
        },
        formatedProducts() {
            let products = [];
            let appears = [];
            for (let key in this.choosed_products) {
                let product = this.choosed_products[key];
                let is_promotion = appears.includes(product.code);
                if(!is_promotion){
                    appears.push(product.code);
                }
                let sell_type = is_promotion ? "promotion" : "selled";
                products.push(product.code + ":" + product.count + ":" + product.discount + ":" + sell_type + ":" + product.price);
            }
            return products;
        }
    },
});