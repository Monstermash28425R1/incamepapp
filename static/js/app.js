$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        function getCookie(name) {
            var cookieValue = null;
            if (document.cookie && document.cookie != '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = jQuery.trim(cookies[i]);
                    // Does this cookie string begin with the name we want?
                    if (cookie.substring(0, name.length + 1) == (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }
            return cookieValue;
        }
        if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
            // Only send the token to relative URLs i.e. locally.
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    }
});


String.prototype.formatStr = function (data = {}) {
    let str = this;
    for (i in data) {
        let arr = str.split("{{" + i + "}}");
        str = arr.join(data[i]);
    }
    return str;
}
$("#client_selector").select2({
    placeholder: "Seleccione Codigo, Nombre comercial, Razón social o RUC",
    ajax: {
        url: '/pedidos/get/clients',
        dataType: 'json',
        data: function (params) {
            var query = {
                search: params.term,
                page: params.page || 1
            }

            // Query parameters will be ?search=[term]&page=[page]
            return query;
        }
    },
    theme: "bootstrap4",
    minimumInputLength: 1,
}).on("change.select2", (e) => {
    fetch("/pedidos/get/client/" + e.currentTarget.value)
        .then(response => response.json())
        .then((data) => {
            $("#client-name").text(data.name);
            $("#client-ruc").text(data.ruc);
            $("#client-rif").text(data.rif);
            $("#client-observation").text(data.observation || "-");
            $("#client-phone").text(data.tel);
            $("#client-email").val(data.email);

            $("#address-country").text(data.address.country);
            $("#address-province").text(data.address.province);
            $("#address-city").text(data.address.city);
            $("#address-zone").text(data.address.zone);
            $("#address-parish").text(data.address.parish);
            $("#address-street").text(data.address.street);
            $("#address-number").text(data.address.number);
            $("#address-interception").text(data.address.interception);

        });
});

$("#seller_selector").select2({
    ajax: {
        url: '/pedidos/get/sellers',
        dataType: 'json',
        data: function (params) {
            var query = {
                search: params.term,
                page: params.page || 1,
            }

            // Query parameters will be ?search=[term]&page=[page]
            return query;
        },

    },
    theme: "bootstrap4",
    minimumInputLength: 1,
});

$("#business_selector").select2({
    placeholder: "Seleccione establecimiento",
    ajax: {
        url: '/pedidos/get/business',
        dataType: 'json',
        data: function (params) {
            var query = {
                search: params.term,
                page: params.page || 1,
                client: $("#client_selector").val()
            }

            // Query parameters will be ?search=[term]&page=[page]
            return query;
        },

    },
    theme: "bootstrap4",
    minimumInputLength: 0,
}).on("change.select2", (e) => {
    $("#client-deliver-address").text(e.currentTarget.value);
});

$("#transport_selector").select2({
    placeholder: "Seleccione Transporte",
    ajax: {
        url: '/pedidos/get/transports',
        dataType: 'json',
        data: function (params) {
            var query = {
                search: params.term,
                page: params.page || 1
            }

            // Query parameters will be ?search=[term]&page=[page]
            return query;
        }
    },
    theme: "bootstrap4",
    minimumInputLength: 1,
});


$(".search-product-form").submit((e) => {
    e.preventDefault();
    let self = e.currentTarget;
    let data = new FormData(self);
    let search = [];

    for (let pair of data.entries()) {
        search.push(pair[0] + "=" + pair[1]);
    }
    let url = self.action + "?" + search.join("&");

    $.ajax({
        url: url,
        type: self.method,
    }).done((response) => {
        repaintProductList(response.page);
        repaintProductPagination(response);
    });
});



$("#clientSearchModal").on("shown.bs.modal", (e) => {
    $(".search-product-form").submit();
}).on("hide.bs.modal", (e) => {
    // repaintChoosedProducts();
});

let choosed_products = [];
let loaded_products = {};

var formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
});

function repaintChoosedProducts() {
    let result = "";
    if (choosed_products.length > 0) {
        let full_price = 0;
        let full_discount = 0;
        let full_total = 0;
        let full_iva = 0;
        let iva_discount_price = 0;
        for (let idx in choosed_products) {
            let product = choosed_products[idx].product;
            let is_promotion = choosed_products[idx].is_promotion;

            if (is_promotion) {
                product.price = 0;
                product.sell_price = 0;
                product.discount = 0;
            }
            let count = choosed_products[idx].count;
            let price = product.price;
            let discount = product.price * (product.discount / 100);
            product.sell_price = (price - discount);
            product.total = (product.sell_price * count);

            full_price += product.price * count;
            full_discount += discount * count;
            //precio con descuento
            discount_price = full_price - full_discount;
            if (product.iva > 0) {
                iva_discount_price += product.sell_price * count;
            }
            console.log({
                iva_discount_price,
                discount_price
            })
            full_iva += iva_discount_price * product.iva;
            let row = `
            <tr>
                <td><button class="btn btn-sm btn-outline-danger" data-remove="{{idx}}">X</button></td>
                <td>{{code}}</td>
                <td>{{name}}</td>
                <td><input  type="number" min="1" step="0.01" class="form-control m-0 modifier product-counter" id="{{idx}}" value="{{count}}" required></td>
                <td>{{measure}}</td>
                <td>
                    <div class="input-group">
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">$</span>
                            <input type="number" min="0" step="0.01" max="{{price}}"class="form-control m-0 modifier product-price" id="{{idx}}" value="{{price}}" required>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="input-group">
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">%</span>
                            <input  type="number" min="0" step="0.01" max="100" class="form-control m-0 modifier product-discount" id="{{idx}}" value="{{discount}}" required>                                </div>
                    </div>
                </td>
                <td>{{sell_price}}</td>
                <td>{{total}}</td>
            </tr>
            `;
            let rowNotEditable = `
            <tr>
                <td><button class="btn btn-sm btn-outline-danger" data-remove="{{idx}}">X</button></td>
                <td>{{code}}</td>
                <td>{{name}}</td>
                <td><input  type="number" min="1" step="0.01" class="form-control m-0 modifier product-counter" id="{{idx}}" value="{{count}}" required></td>
                <td>{{measure}}</td>
                <td>{{price}}</td>
                <td>
                    <div class="input-group">
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="basic-addon1">%</span>
                            <input  type="number" min="0" step="0.01" max="100" class="form-control m-0 modifier product-discount" id="{{idx}}" value="{{discount}}" required>                                </div>
                    </div>
                </td>
                <td>{{sell_price}}</td>
                <td>{{total}}</td>
            </tr>
            `;
            let obj = {
                idx,
                count,
                code: product.code,
                name: product.name,
                measure: product.measure,
                price: "$" + product.price.toFixed(4),
                discount: product.discount,
                sell_price: "$" + product.sell_price.toFixed(4),
                total: formatter.format(product.total)
            }
            let obj1 = {
                idx,
                count,
                code: product.code,
                name: product.name,
                measure: product.measure,
                price: product.price,
                discount: product.discount,
                sell_price: formatter.format(product.sell_price),
                total: formatter.format(product.total)
            }
            result += is_promotion ? row.formatStr(obj1) : rowNotEditable.formatStr(obj);

        }
        $("#total-gross").text(formatter.format(full_price));
        $("#total-discount").text(formatter.format(full_discount));
        $("#total-sell-value").text(formatter.format(discount_price));
        $("#total-iva").text(formatter.format(full_iva));
        $("#total-value").text(formatter.format(discount_price + full_iva));
    }

    $("#checkout-table tbody").html(result);


    $("[type=number]").unbind("keydown").on("keydown", (e) => {
        if (e.keyCode == 13 || e.which == 13) {
            return false;
        }
    });
    $(".product-counter").on("change", (e) => {
        let key = e.currentTarget.id;
        let count = choosed_products[key].count = parseInt(e.currentTarget.value);
        let code = choosed_products[key].product.code;
        $.ajax({
            type: "POST",
            url: "/pedidos/get/discount",
            data: {
                code: code,
                amount: count
            },
            success: function (response) {
                choosed_products[key].discount = response.discount;
                // repaintChoosedProducts();
            }
        });
    });
    $(".product-discount").on("change", (e) => {
        let key = parseInt(e.currentTarget.id);
        let product = choosed_products[key].product;
        product.discount = parseFloat(e.currentTarget.value);
        let discount = product.price * (product.discount / 100);
        choosed_products[key].product.sell_price = (choosed_products[key].product.price - discount).toFixed(2);
        // repaintChoosedProducts();
    });
    $(".product-price").on("change", (e) => {
        let key = e.currentTarget.id;
        choosed_products[key].product.sell_price = parseFloat(e.currentTarget.value);
        choosed_products[key].product.discount = 0;
        // repaintChoosedProducts();
    });
    $("[data-remove]").on("click", (e) => {
        let target = $(e.currentTarget).data("remove");
        choosed_products.splice(target, 1);
        // repaintChoosedProducts();
    });
}


function repaintProductPagination(response) {
    let $paginator = $("#productsPaginator");
    let page = '<li class="page-item" data-value="{{index}}"><a class="page-link" href="{{index}}">{{index}}</a></li>'
    $paginator.empty();
    if (response.pages > 1) {
        for (let i = response.prev - 1; i <= response.next + 1; i++) {
            if (i > 0 && i <= response.pages) {
                $page = $(page.formatStr({
                    index: i
                }));
                $page.on("click", (ev) => {
                    ev.preventDefault();
                    $("#productForm").find("[name=page]").val($(ev.currentTarget).data("value"));
                    $("#productForm").submit();
                });
                if (i == response.current) {
                    $page.addClass("active");
                }
                $paginator.append($page);
            }
        }
    }
}

function repaintProductList(products) {
    let result = '';
    for (let product of products) {
        loaded_products[product.code] = product;
        let str = `
        <tr>
            <td>
                <button type="button" class="btn btn-sm btn-primary" data-product="{{code}}">Añadir</button>
            </td>
            <td>{{code}}</td>
            <td>{{name}}</td>
        </tr>`;
        str = str.formatStr(product);
        result += str;
    }
    $("#product-table tbody").html(result);
    $("[data-product]").on("click", (e) => {
        let $self = $(e.currentTarget);
        let key = $self.data("product");
        let product = JSON.parse(JSON.stringify(loaded_products[key]));
        console.log(product)
        if (product.status) {
            let count = app.choosed_products.filter(p => p.code == key).length;
            let is_promotion = count > 0;
            let limit = 2;
            if (count < limit) {
                app.choosed_products.push({
                    count: 1,
                    is_promotion: is_promotion,
                    ...product
                });
            }
        } else {
            alert(`El producto ${product.name} no está disponible para la venta en este momento.`)
        }
    });
}


$("#save").submit((e) => {
    // e.preventDefault();
    let products = app.formatedProducts();
    console.log(products);
    $("[name=client]").val($("#client_selector").val());
    $("[name=products]").val(products);

});
$("#toggle-sidebar").on("click", (e) => {
    $("#sidebar").toggleClass("active");
    let shown = $("#sidebar").hasClass("active");
    if (shown) {
        $(e.currentTarget).html('<i class="fa fa-arrow-left"></i>')
    } else {
        $(e.currentTarget).html('<i class="fa fa-arrow-right"></i>')
    }
});

$(".sidebar-item").on("click", (e) => {
    $(".sidebar-item").removeClass("active");
    $(e.currentTarget).addClass("active");
    let shown = $("#sidebar").hasClass("active");
    if (!shown) {
        $("#sidebar").addClass("active");
    }
});

$("input[type=text]").on("keyup, change", (e) => {
    $(e.currentTarget).val($(e.currentTarget).val().toUpperCase());
});

$(".checkbox-group input[type=checkbox]").on("change", (e) => {
    $(e.currentTarget).closest(".checkbox-group").find("input[type=checkbox]").not(e.currentTarget).prop("checked", false);
});

document.getElementById("send_to_email").onclick = (e) => {
    let self = e.currentTarget;
    e.preventDefault();
    $(self).addClass("disabled");
    fetch("/send/report/" + self.dataset.target).then(response => response.json()).then((content) => {
        if (content.success) {
            showToast("El repote fue enviado exitosamente.")
        } else {
            showToast("El reporte no pudo ser enviado.", "danger")
        }
        console.log(content)
        $(self).removeClass("disabled");
    }).catch((ex) => {
        console.log(ex);
        showToast("El reporte no pudo ser enviado.", "danger")
        $(self).removeClass("disabled");
    });
};

function showToast(message, level = "success", duration = 2000) {
    let $el = $("#toast");
    $el.addClass("bg-" + level);
    $el.html(message);
    $el.show(200).delay(duration).hide(200);
}