from django.db import models

# TODO Photo mounting
class Seller(models.Model):
    id = models.IntegerField(db_column='Registro', primary_key=True, auto_created=True)
    identification = models.CharField(db_column='Cedula', max_length=45)
    names = models.CharField(db_column='Nombres', max_length=45)
    last_names = models.CharField(db_column='Apellidos', max_length=45)
    address = models.CharField(db_column='Direccion', max_length=45)
    phone = models.CharField(db_column='Telefono', max_length=45)
    entry = models.CharField(db_column='Ingreso', max_length=45)
    exitt = models.CharField(db_column='Salida', max_length=45)
    photo = models.CharField(db_column='Foto', max_length=45)
    username = models.CharField(db_column='Usuario', max_length=45, default="")

    class Meta:
        db_table = "tblcxc_vendedor"