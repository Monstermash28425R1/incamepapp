from django.urls import path
from . import views
from . import emails


urlpatterns = [
    path('', views.index, name="orders_list"),
    path('<int:pk>/ver', views.show),
    path('<int:pk>/eliminar', views.delete),
    path('registrar', views.register,name="register_order"),
    path('<int:pk>/editar', views.update,name="order_edit"),
    path('get/clients',views.get_clients,name="get_clients"),
    path('get/business',views.get_business,name="get_business"),
    path('get/client/<client>',views.get_client,name="get_client"),
    path('get/transports',views.get_transports,name="get_transports"),
    path('get/sellers',views.get_sellers,name="get_sellers"),
    path('get/products',views.get_products,name="get_products"),
    path('get/discount',views.get_product_discount,name="get_product_discount"),
]
