## EMAIL REQUIREMENTS
from django.core.mail import send_mail, EmailMessage
from django.conf import settings
from django.template.loader import render_to_string
from django.utils.html import strip_tags
base_dir = settings.BASE_DIR

## DB objects
from orders.models import Order, Product

## PDF generator 
from io import BytesIO
from django.http import HttpResponse
from django.template.loader import get_template
import xhtml2pdf.pisa as pisa
import os
from datetime import datetime

def generate_order_pdf(order:dict):
    template = get_template("pdf/pdf-template.html")
    html = template.render(order)
    buffer = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")),buffer)
    if not pdf.err:
        return buffer
    return False

def save_order_pdf(order:dict):
    buffer = generate_order_pdf(order)
    if buffer:
        file_path = f"tmp/{order['title']}.pdf"
        file = open(file_path ,"wb")
        file.write(buffer.getvalue())
        file.close()
        return file_path
    return False

def send_order_details(to_email:str,order:dict):
    file_path = save_order_pdf(order)
    from_email = settings.SELLER_EMAIL

    html_text = render_to_string("emails/email-template.html",  order)
    plain_text = strip_tags(html_text)
    email_title = f"Notas de pedido #{order['order'].number}"

    if not file_path:
        return False

    msg = EmailMessage(email_title,html_text, from_email, [to_email])
    msg.content_subtype = "html"
    try:
        file = open(file_path,"rb")
        filename = file.name.split("/")[-1]
        msg.attach(filename,file.read(),"application/pdf")
        msg.send()
        file.close()
        os.remove(file_path)
        return True
    except Exception as ex:
        print(ex)
        return False