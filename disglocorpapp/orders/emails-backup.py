from django.core.mail import send_mail
from django.conf import settings
from django.template.loader import render_to_string
from django.utils.html import strip_tags

from weasyprint import HTML
from weasyprint.fonts import FontConfiguration

from io import BytesIO

base_dir = settings.BASE_DIR

def export_pdf(request):

    context = {}
    html = render_to_string("report/report-pdf.html", context)

    response = HttpResponse(content_type="application/pdf")
    response["Content-Disposition"] = "inline; report.pdf"

    font_config = FontConfiguration()
    HTML(string=html).write_pdf(response, font_config=font_config)
    return response

def send_order_details(toemail, context):

    titleStr = "Payment notification"

    html_text = render_to_string(
        base_dir + str('/templates/emails/order-details.html'),  {'order': context})
    # plain_text = strip_tags(html_text)

    # response = HttpResponse(content_type="application/pdf")
    # response["Content-Disposition"] = "inline; nota-pedido.pdf"

    font_config = FontConfiguration()
    # html = HTML(string=html).write_pdf(response, font_config=font_config)
    
    file = BytesIO()
    html = HTML(string=html).write_pdf(file, font_config=font_config)

    sentTo = {toemail}
    if send_mail(titleStr, plain_text,
                 'ventas@disglocorp.com', sentTo, html_message=html_text):
        return True
    return False
