from django.shortcuts import render
from django.shortcuts import redirect
from users.managers import CustomAuthManager
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse,HttpResponseNotFound, HttpResponseForbidden
from .models import Order, OrderDetail, Product, Transport,Business
from clients.models import Client
from sellers.models import Seller
from django.core.serializers import serialize
from django.db.models import Q
import json
from datetime import datetime
from random import randint
from datetime import date

import orders.emails as email_manager

from django.conf import settings
base_dir = settings.BASE_DIR


def send_pdf(request,order_id):
    auth_manager = CustomAuthManager(request)
    if (auth_manager.is_authenticated() == False):
        return redirect('/auth/login')

    us = auth_manager.user()
    seller = us.seller
    order = Order.objects.filter(number=order_id).first()

    if order.seller != seller and not us.is_admin:
        return HttpResponseForbidden()


    products = []
    title = f"Nota de pedido #{order.number} - {order.client.social_reason}"
    
    gross_price = 0.0
    discount = 0.0
    sell_price = 0.0
    iva = 0.0
    total = 0.0
    iva_price = 0.0
    for details in order.details.all():
        product = details.product
        count = int(details.amount)
        
        gross_price += float(details.unit_price) * count
        discount += float(details.value_discount) * count
        price = 0 if details.unit_price == 0 else product.get_unit_price()
        if product.iva == "SI":
            iva_price += float(details.discount_price) * count
        products.append({
            'code':product.code,
            'name':product.name,
            'count':count,
            'price':price,
            'discount_percent':details.percent_discount,
            'u_m': str(product.measure.shortname),
            'pvp':details.discount_price,
            'total': details.total,
            'has_iva':float(details.iva_value) > 0 if details.iva_value else False,
        })

    sell_price = gross_price - discount
    iva = iva_price * 0.12
    total = sell_price + iva
    
    today = datetime.now()

    summary = {
        'price': round(gross_price,2),
        'discount': round(discount,2),
        'sell_price': round(sell_price,2),
        'iva': round(iva,2),
        'total': round(total,2)
    }
    obj = {
        'title':title, 
        'today':today, 
        'order':order, 
        'products':products,
        'summary':summary,
        'base_dir':base_dir
    }

    to_email = order.email
    sent = email_manager.send_order_details(to_email,obj)
    return JsonResponse({"success":sent})

def get_pdf(request,order_id):
    auth_manager = CustomAuthManager(request)
    if (auth_manager.is_authenticated() == False):
        return redirect('/auth/login')

    us = auth_manager.user()
    seller = us.seller
    order = Order.objects.filter(number=order_id).first()

    if order.seller != seller and not us.is_admin:
        return HttpResponseForbidden()


    products = []
    title = f"Nota de pedido #{order.number} - {order.client.social_reason}"
    
    gross_price = 0.0
    iva_price = 0.0
    discount = 0.0
    sell_price = 0.0
    iva = 0.0
    total = 0.0
    
    for details in order.details.all():
        product = details.product
        count = int(details.amount)
        
        gross_price += float(details.unit_price) * count
        discount += float(details.value_discount) * count
        price = 0 if details.unit_price == 0 else product.get_unit_price()
        print(product.measure.shortname)

        if product.iva == "SI":
            iva_price += float(details.discount_price) * count

        products.append({
            'code':product.code,
            'name':product.name,
            'count':count,
            'price':price,
            'discount_percent':details.percent_discount,
            'u_m': str(product.measure.shortname),
            'pvp':details.discount_price,
            'total': details.total,
            'has_iva':float(details.iva_value) > 0 if details.iva_value else False,
        })
    sell_price = gross_price - discount
    iva = iva_price * 0.12
    total = sell_price + iva
    today = datetime.now()

    summary = {
        'price':round(gross_price,2),
        'discount':round(discount,2),
        'sell_price':round(sell_price,2),
        'iva':round(iva,2),
        'total':round(total,2)
    }
    obj = {
        'title':title, 
        'today':today, 
        'order':order, 
        'products':products,
        'summary':summary,
        'base_dir':base_dir
    }


    buffer = email_manager.generate_order_pdf(obj)

    if buffer: #if buffer is not False
        return HttpResponse(buffer.getvalue(),content_type="application/pdf")
        
    #else
    return HttpResponse("Error Rendering PDF", status=400)

def test_email(request,order_id):
    if request.method != "GET":
        return HttpResponseBadRequest()
    
    template = "emails/email-template.html"
    order = Order.objects.filter(number=order_id).first()
    return render(request,template,{"order":order}) 

def _create(request):
    auth_manager = CustomAuthManager(request)
    if (auth_manager.is_authenticated() == False):
        return redirect('/auth/login')
    
    
    _client = request.POST.get("client")
    email = request.POST.get("client_email")
    _transport = request.POST.get("transport")
    request_by = request.POST.get("request_by").upper()
    payment_method = request.POST.get("payment_method","CONTADO")
    delivery_address = request.POST.get("delivery_address","")
    comment = request.POST.get("comment","")
    
    client = Client.objects.filter(pk=_client).first()
    transport = Transport.objects.filter(pk=_transport).first()
    product_list = request.POST.get("products").split(",")


    ## payment method, 0 == CONTADO, 1 == CREDITO
    credit_days = 1 if payment_method == "CONTADO" else 30
    payment = "CONTADO" if payment_method == "CONTADO" else "CREDITO"

    user = auth_manager.user()
    seller = user.seller

    if not delivery_address or delivery_address == "":
        delivery_address = f"{client.street}, {client.street_number}, {client.interception}"

    new_order = Order(
        date = datetime.now(), 
        order_date = datetime.now(),
        payment = payment,
        deadline = credit_days,
        request_by = request_by, 
        delivery_address = delivery_address, 
        client = client,
        transport = transport,
        seller = seller,
        email = email,
        comment=comment,
        user=user.name
        # consignation 
        # status = 
        # created_at = 
    )
    new_order.save()
    line = 1
    for p in product_list:
        # p is code:units:discount:process
        
        code = p.split(":")[0] 
        units = p.split(":")[1]
        discount = float(p.split(":")[2])
        process = p.split(":")[3]
        print(p.split(":"))

        product = Product.objects.filter(code=code).first()
        price = 0 if process =="promotion" else product.get_unit_price()

        od = OrderDetail(
            line = line,
            product = product,
            order = new_order,
            amount = units,
            user = auth_manager.user().name,
            percent_discount=discount,
            unit_price = price
        )  
        od.save()
        line = line + 1
    new_order.save()
    return redirect(f"/pedidos/{new_order.pk}/ver")


def show(request, pk):
    auth_manager = CustomAuthManager(request)
    if (auth_manager.is_authenticated() == False):
        return redirect('/auth/login')
        #return HttpResponse(auth_manager.user())
    order = Order.objects.filter(number=pk).first()
    if not order:
        return HttpResponseNotFound()

    products = []

    total = 0
    subtotal = 0
    iva = 0
    
    us = auth_manager.user()

    if order.seller != us.seller and not us.is_admin:
        return HttpResponseForbidden()

    for details in order.details.all():
        product =  details.product
        subtotal += float(details.amount * details.unit_price)
        total += float(details.total) + details.get_iva_value()
        count = int(details.amount)

        price = 0 if details.unit_price == 0 else product.get_unit_price()
        iva_percent = float(Business.objects.first().iva_percent)
        iva = iva_percent if product.iva == "SI" else 0
        products.append({
            "count":count,
            "code":product.code,
            "name":product.name,
            "price":float(price),
            "discount":float(details.percent_discount),
            "total":str(round(float(details.total)+details.get_iva_value(),2)),
            "sell_price":str(round(details.unit_price,2)),
            "iva":iva,
            "measure":product.measure.shortname
        })
    us = auth_manager.user()
    return render(request, 'app/orders/view.html', {'order': order,"products":json.dumps(products),'auth_user': us})


def _showRegisterForm(request):
    auth_manager = CustomAuthManager(request)
    if (auth_manager.is_authenticated() == False):
        return redirect('/auth/login')
        #return HttpResponse(auth_manager.user())
    register_id = len(Order.objects.all()) +1
    us = auth_manager.user()
    today = date.today().strftime("%d/%m/%Y")
    return render(request, 'app/orders/register.html',{"register_id":register_id, 'auth_user': us, "today": today})

def _showEditionForm(request,pk):
    auth_manager = CustomAuthManager(request)
    if (auth_manager.is_authenticated() == False):
        return redirect('/auth/login')
    order = Order.objects.filter(number=pk).first()
    if not order:
        return HttpResponseNotFound()
    
    us = auth_manager.user()

    if order.seller != us.seller and not us.is_admin and order.status != "INGRESADO":
        return HttpResponseForbidden()

    details = order.details.first()
    total = 0
    subtotal = 0
    iva = 0
    products =[]
    for details in order.details.all():
        product = details.product
        subtotal += float(details.amount * details.unit_price)
        total += float(details.total) + details.get_iva_value()
        iva += details.get_iva_value()
        count = int(details.amount)
        
        price = 0 if details.unit_price == 0 else product.get_unit_price()
        iva_percent = float(Business.objects.first().iva_percent)
        iva = iva_percent if product.iva == "SI" else 0
        products.append({
            "count":count,
            "id":product.pk,
            "code":product.code,
            "name":product.name,
            "price":float(price),
            "discount":float(details.percent_discount),
            "total":str(round(float(details.total)+details.get_iva_value(),2)),
            "sell_price":str(round(details.unit_price,4)),
            "iva":iva,
            "measure":product.measure.shortname
        })
        
    us = auth_manager.user()
    client = order.client

    direction = client.street + str(", ") + client.street_number + str(", ") + client.interception

    return render(request, 'app/orders/edit.html',{"order":order,"direction":direction,"products":json.dumps(products), 'auth_user': us})

def _edit(request,pk):
    auth_manager = CustomAuthManager(request)
    if not auth_manager.is_authenticated():
        return redirect('/auth/login')
    
    _client = request.POST.get("client")
    email = request.POST.get("client_email")
    _transport = request.POST.get("transport")
    request_by = request.POST.get("request_by").upper()
    payment_method = request.POST.get("payment_method","CONTADO")
    delivery_address = request.POST.get("delivery_address","")

    status = request.POST.get("status","INGRESADO")

    comment = request.POST.get("comment","")
 
    client = Client.objects.filter(pk=_client).first()
    transport = Transport.objects.filter(pk=_transport).first()
    product_list = request.POST.get("products").split(",")

    ## payment method, 0 == CONTADO, 1 == CREDITO
    credit_days = 1 if payment_method == "CONTADO" else 30
    payment = "CONTADO" if payment_method == "CONTADO" else "CREDITO"


    user = auth_manager.user()
    seller = user.seller


    if not delivery_address or delivery_address == "":
        delivery_address = f"{client.street}, {client.street_number}, {client.interception}"
    if not user.is_admin:
        status = "INGRESADO"

    order = Order.objects.filter(pk=pk).first()

    if not order:
        return HttpResponseNotFound()

    if order.status == "APROBADA" and not user.is_admin:
        return HttpResponseForbidden()
    order.status = status
    order.user = user.name
    order.date = datetime.now()
    order.order_date = datetime.now()


    order.payment = payment
    order.deadline = credit_days



    order.email = email
    order.request_by = request_by
    order.delivery_address = delivery_address
    order.client = client
    order.transport = transport
    order.seller = seller
    order.comment = comment
    order.details.all().delete()

    line = 1
    for p in product_list:
        
        code = p.split(":")[0] 
        units = p.split(":")[1]
        discount = float(p.split(":")[2])
        process = p.split(":")[3]
        print(p.split(":"))

        product = Product.objects.filter(code=code).first()
        price = 0 if process =="promotion" else product.get_unit_price()
        od = OrderDetail(
            product = product,
            line = line,
            order = order,
            # unit_price = product.get_unit_price(),
            unit_price = price,
            amount = units,
            user = auth_manager.user().name,
            percent_discount= discount
        )
        od.save()
        line = line + 1
    order.save()
    return redirect('/pedidos/')


##
def get_client(request,client):
    if client:
        c = Client.objects.filter(pk=client).first()
        if(c):
            return JsonResponse({
                "name":c.name,
                "ruc":c.identification,
                "rif":c.social_reason,
                "tel":c.phone,
                "email":c.mail,
                "observation":c.contact_2,
                "address":{
                    "street":c.street,
                    "number":c.street_number,
                    "interception":c.interception,
                    "country" :c.country.name,
                    "province":c.province.name,
                    "city":c.city.name,
                    "parish":c.parish.name,
                    "zone":c.zone.name
                }
            })  
        else:
            return HttpResponseNotFound()

    else:
        return HttpResponseNotFound()
def get_clients(request):
    if not request.is_ajax() or request.method != "GET":
        return HttpResponseBadRequest()
    
    page_index = int(request.GET.get("page",1))
    page_count = 10 # Puede ser un campo dinamico para que el usuario decida cuantos rows mostrar por pagina
    filter = request.GET.get("search",None)


    if filter:
            products = Client.objects.filter(Q(identification__icontains=filter) | Q(id=filter) | Q(social_reason__icontains=filter)).order_by("id")
    else:
        products = Client.objects.all().order_by("id")

    pages = Paginator(products, page_count)
    page = None
    if pages.num_pages > 0 and page_index <= pages.num_pages:
        page = pages.page(page_index)
    
    data = []

    if page:
        for i in page:
            data.append({
                "id":i.pk,
                "text":i.social_reason,
                "credit_limit":i.credit_limit,
                "credit_days":i.credit_days
            })
    return JsonResponse({
        "results":data,
        "pagination":{
            "more": page.has_next()
        }
    })

def get_business(request):
    if not request.is_ajax() or request.method != "GET":
        return HttpResponseBadRequest()
    
    page_index = int(request.GET.get("page",1))
    client = int(request.GET.get("client"))
    page_count = 10 # Puede ser un campo dinamico para que el usuario decida cuantos rows mostrar por pagina

    client = Client.objects.filter(pk=client).first()

    address = client.street+ ", " + client.street_number + ", " + client.interception
    data = [{"id":address,"text":address}]
    return JsonResponse({
        "results":data,
        "pagination":{
            "more": False
        }
    })

def get_sellers(request):
    if not request.is_ajax() or request.method != "GET":
        return HttpResponseBadRequest()
    
    page_index = int(request.GET.get("page",1))
    page_count = 10 # Puede ser un campo dinamico para que el usuario decida cuantos rows mostrar por pagina
    filter = request.GET.get("search",None)


    if filter:
        products = Seller.objects.filter(Q(names__icontains=filter)|Q(last_names__icontains=filter)|Q(identification__icontains=filter)).order_by("id")
    else:
        products = Seller.objects.all().order_by("id")

    pages = Paginator(products, page_count)
    page = None
    if pages.num_pages > 0 and page_index <= pages.num_pages:
        page = pages.page(page_index)
    
    data = []

    if page:
        for i in page:
            data.append({
                "id":i.pk,
                "text":i.names
            })
    return JsonResponse({
        "results":data,
        "pagination":{
            "more": page.has_next()
        }
    })

def get_transports(request):
    if not request.is_ajax() or request.method != "GET":
        return HttpResponseBadRequest()
    
    page_index = int(request.GET.get("page",1))
    page_count = 10 # Puede ser un campo dinamico para que el usuario decida cuantos rows mostrar por pagina
    filter = request.GET.get("search",None)


    if filter:
        products = Transport.objects.filter(Q(name__icontains=filter)|Q(plate__icontains=filter)).order_by("id")
    else:
        products = Transport.objects.all().order_by("id")

    pages = Paginator(products, page_count)
    page = None
    if pages.num_pages > 0 and page_index <= pages.num_pages:
        page = pages.page(page_index)
    
    data = []

    if page:
        for i in page:
            data.append({
                "id":i.pk,
                "text":i.name
            })
    return JsonResponse({
        "results":data,
        "pagination":{
            "more": page.has_next()
        }
    })

def get_products(request):
    if not request.is_ajax() or request.method != "GET":
        return HttpResponseBadRequest()
    
    page_index = int(request.GET.get("page",1))
    page_count = int(request.GET.get("page_count",10))# Puede ser un campo dinamico para que el usuario decida cuantos rows mostrar por pagina
    search = request.GET.get("search",None)

    if search:
        _products = Product.objects.filter(Q(name__icontains=search)|Q(code__icontains=search)).order_by("-created_at")
    else:
        _products = Product.objects.all().order_by("-created_at")

    products = []
    for p in _products:
    #    if p.get_unit_price() > 0:
        products.append(p)
    
    pages = Paginator(products, page_count)
    page = None

    if page_index > pages.num_pages:
        page_index = 1

    if pages.num_pages > 0 and page_index <= pages.num_pages:
        page = pages.page(page_index)
    
    data = []
    if page:
        for i in page:


            price = i.get_unit_price()
            iva_percent = float(Business.objects.first().iva_percent)
            iva = iva_percent if i.iva == "SI" else 0
            data.append({
                "id":i.pk,
                "code":i.code,
                "name":i.name,
                "discount":0,
                "sell_price":price,
                "price":price,
                "iva":iva,
                "measure":i.measure.shortname
            })
            

    return JsonResponse({
        "page":data,
        "pages":pages.num_pages,
        "current":page_index,
        "next": page.next_page_number() if page.has_next() else 0,
        "prev": page.previous_page_number() if page.has_previous() else 0
    })
#!

def get_product_discount(request):
    if request.method != "POST" or not request.is_ajax():
        return HttpResponseBadRequest()
    
    code = request.POST.get("code")
    amount = request.POST.get("amount")

    product = Product.objects.filter(code=code).first()
    try:
        discount = product.o_details.first().get_discount_percent(amount)
    except:
        discount = 0
    return JsonResponse({"discount":discount})

def register(request):
    if request.method == "POST":
        return _create(request)
    return _showRegisterForm(request)

# TODO Paginar
def index(request):
    auth_manager = CustomAuthManager(request)
    if (auth_manager.is_authenticated() == False):
        return redirect('/auth/login')

    user = auth_manager.user()
    if user.is_admin:
        orders_list = Order.objects.all().order_by('-id') # aqui se filtran los pedidos hechos por el vendedor actual
    else:
        orders_list = Order.objects.filter(seller=user.seller).order_by('-id') # aqui se filtran los pedidos hechos por el vendedor actual

    page = request.GET.get('page', 1)
    paginator = Paginator(orders_list, 16)
    try:
        orders = paginator.page(page)
    except PageNotAnInteger:
        orders = paginator.page(1)
    except EmptyPage:
        orders = paginator.page(paginator.num_pages)
   
    us = auth_manager.user()
    return render(request, 'app/orders/index.html', {'orders': orders,'auth_user': us})


def update(request,pk):
    auth_manager = CustomAuthManager(request)
    if (auth_manager.is_authenticated() == False):
        return redirect('/auth/login')
    user = auth_manager.user()

    if request.method == "POST":
        return _edit(request,pk)
    return _showEditionForm(request,pk)

def delete(request, pk):
    auth_manager = CustomAuthManager(request)
    if (auth_manager.is_authenticated() == False):
        return redirect('/auth/login')
    user = auth_manager.user()

    if request.method != "POST":
        return HttpResponseBadRequest()
    
    order = Order.objects.get(pk=pk)
    
    if not user.is_admin and order.seller != user.seller:
        return HttpResponseForbidden()

    order.products.clear()
    # TODO Detach products (Deleting OrderDetails)
    order.delete()
    return redirect('/pedidos/')
