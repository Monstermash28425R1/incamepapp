from django.db import models

from clients.models import Client
from sellers.models import Seller
from datetime import datetime


# TODO completar relación con provincia
class Transport(models.Model):
    id = models.IntegerField(db_column='Registro', primary_key=True, auto_created=True)
    ruc = models.CharField(db_column='Ruc', max_length=45)
    name = models.CharField(db_column='Nombre', max_length=45)
    address = models.CharField(db_column='Direccion', max_length=90)
    phone = models.CharField(db_column='Telefono', max_length=45)
    plate = models.CharField(db_column='Placa', max_length=10)
    #state - Provincia

    class Meta:
        db_table = "tblfac_transporte"

class Price(models.Model):
    id = models.AutoField(db_column="Registro",primary_key=True)
    price = models.FloatField(db_column='Precio')
    #product = models.ForeignKey(Product, db_column="Producto", on_delete=models.CASCADE, related_name="price")
    product = models.IntegerField(db_column="Producto")
    class Meta:
        db_table = "tblfac_productoprecio"

# TODO store = Bodega relationship
# TODO group = Grupo Relationship
# TODO subgroup = SubGrupo Relationship
# TODO line = LineaProducto Relationship
# TODO measure Medida Relationship
# TODO ubication = Ubicacion Relationship
# TODO machine = Maquina Relationship
# TODO mold =  Molde Relationship

class ProductMeasure(models.Model):
    id = models.AutoField(db_column="Registro",primary_key=True)
    shortname = models.CharField(db_column="Abreviatura", unique=True,max_length=45,default="")
    name = models.CharField(db_column="Nombre",max_length=45,default="")
    type = models.CharField(db_column="Tipo",max_length=45,default="")
    
    def __str__(self):
        return self.name
    
    class Meta:
        db_table = 'tblinv_medida'


class Product(models.Model):
    id = models.IntegerField(db_column='Registro', primary_key=True, auto_created=True)
    #details = models.name = models.ForeignKey(OrderDetail, related_name='products', on_delete=models.CASCADE)
    code = models.CharField(db_column='Codigo', max_length=45, unique=True)
    name = models.CharField(db_column='Nombre', max_length=90) # this is a relationhsip ?
    #store = Bodega relationship
    #group = Grupo Relationship
    #subgroup = SubGrupo Relationship
    #line = LineaProducto Relationship
    #measure 
    measure = models.ForeignKey(ProductMeasure,db_column='Medida', related_name='product', on_delete=models.CASCADE)
    
    origin = models.CharField(db_column='Origen', max_length=45)
    iva = models.CharField(db_column='Iva', max_length=45)
    max = models.FloatField(db_column='Maximo')
    min = models.FloatField(db_column='Minimo')
    budgeted = models.FloatField(db_column='Presupuestado')
    pending_pab = models.IntegerField(db_column='PendientePAB')
    #ubication = Ubicacion Relationship
    #machine = Maquina Relationship
    #mold =  Molde Relationship
    production_by_hour = models.IntegerField(db_column='ProduccionHora')
    inventory = models.CharField(db_column='Inventario', max_length=2, default='SI') # original es charArray de 2
    status = models.CharField(db_column='Estatus', max_length=45) # ACTIVO, INACTIVO
    created_at = models.DateTimeField(db_column="Creacion")

    # Appends
    unit_price = 0.0

    # TODO no retornar 0 ?
    def get_unit_price(self):
        price = Price.objects.filter(product=self.id).first()
        if price == None:
            return float(0)
        return float(round(price.price,4))

    def calc_total_price(self, units):
        unit_price = self.get_unit_price()
        total = unit_price * float(units)
        return float(total)

    def __str__(self) -> str:
        return self.name

    class Meta:
        db_table = "tblinv_producto"



class Order(models.Model):
    # id = models.IntegerField(db_column='Registro', primary_key=True, auto_created=True)
    id = models.AutoField(db_column="Registro",primary_key=True)
    number = models.IntegerField(db_column='Numero', unique=True)
    date = models.DateField(db_column='Fecha',auto_now_add=True)
    order_date = models.DateField(db_column='FechaPedido')
    # day = models.ArrayField(
    #     models.CharField(db_column="Dia", max_length=3)
    # )
    day = models.CharField(db_column="Dia", max_length=3)
    payment = models.CharField(db_column='Pago', max_length=45)
    deadline = models.IntegerField(db_column='Plazo')
    request_by = models.CharField(db_column='Solicita',default='', max_length=45)
    delivery_address = models.CharField(db_column='Entrega', max_length=135)

    # 1 Orden pertenece a 1 Client
    client = models.ForeignKey(
        Client,
        db_column='Cliente',
        related_name='orders',
        on_delete=models.CASCADE,
    ) 
    transport = models.ForeignKey(Transport, db_column='Transporte', on_delete=models.CASCADE)

    # 1 Order pertenece a 1 Seller
    seller = models.ForeignKey(Seller, db_column='Vendedor',related_name="orders", on_delete=models.CASCADE)
    consignation = models.CharField(db_column="Consignacion", default="", max_length=2)
    
    ## status
    status = models.CharField(db_column="Estatus", max_length=15, default="INGRESADO") # INGRESADA, APROBADA, ANULADA, FACTURADA, BODEGA
    
    
    
    created_at = models.DateTimeField(db_column="Creacion", auto_now_add=True)

    # Notese que esto NO es una relación oficial, aunque asumo que el comportamiento deseado es que se 
    # coloque aca el usuario que hizo el registro.. Asumo !!
    user = models.CharField(db_column='Usuario', max_length=45)
    
    products = models.ManyToManyField(Product, through='OrderDetail')
    comment = models.CharField(db_column="Comentario",default="", max_length=45)
    email = models.CharField(db_column="Correo",default="", max_length=45)
    def save(self, *args, **kwargs):
        self.number = self.pk
        days = ["LUN","MAR","MIE","JUE","VIE","SAB","DOM"]
        self.day = days[datetime.today().weekday()]
        super(Order, self).save(*args, **kwargs) # Call the real save() method

    class Meta:
        db_table = "tblfac_notapedido"

class OrderDetail(models.Model):
    #id = models.IntegerField(db_column='Registro', primary_key=True, auto_created=True)
    id = models.AutoField(db_column="Registro",primary_key=True)

    order = models.ForeignKey(Order, db_column='NotaPedido',related_name="details", on_delete=models.CASCADE)
    product = models.ForeignKey(Product, db_column='Producto', related_name="o_details", on_delete=models.CASCADE)
    line = models.IntegerField(db_column="Linea")

    amount = models.IntegerField(db_column='Cantidad',default=0)
    invoice_quantity = models.IntegerField(db_column='CantidadFactura',default=0)
    quantity_status = models.CharField(db_column='CantidadEstatus', blank=True, default='', max_length=10)
    
    unit_price = models.FloatField(db_column='PrecioUnitario')
    total = models.FloatField(db_column='Total')
    
    discount_price = models.FloatField(db_column='PrecioDescuento')
    percent_discount = models.FloatField(db_column='DescuentoPorcentaje')
    value_discount = models.FloatField(db_column='DescuentoValor')
    
    pending = models.FloatField(db_column='Pendiente',default=0.0)
    pending_order_store = models.IntegerField(db_column='PendientePedidoBodega', default=0)
    pending_invoice = models.IntegerField(db_column='PendienteFactura', default=0)
    pending_status = models.CharField(db_column='PendienteEstatus', max_length=10, default='', blank=True)

    created_at = models.DateTimeField(db_column="Creacion",default=datetime.now)

    # Notese que esto NO es una relación oficial, aunque asumo que el comportamiento deseado es que se 
    # coloque aca el usuario que hizo el registro.. Asumo !!
    user = models.CharField(db_column='Usuario', max_length=10)

    discount_rate_12 = models.FloatField(db_column='DescuentoTarifa12')
    discount_rate_00 = models.FloatField(db_column='DescuentoTarifa00')

    sale_rate_12 = models.FloatField(db_column='VentaTarifa12')
    sale_rate_00 = models.FloatField(db_column='VentaTarifa00')

    iva_value = models.FloatField(db_column='IvaValor')

    def save(self, *args, **kwargs):
        # TODO save discount data handled by functions

        # precio unitario
        # procentaje de descuento que trae el frontend
        # y la cantidad de productos
        # iva

        self.value_discount = round(self.unit_price * (self.percent_discount / 100),8) # precio unitario menos % descuento = descuento unitario
        self.discount_price = round(self.unit_price - self.value_discount,4) # precio unitario - descuento unitario = precio unitario descontado
        
        self.total = round(float(self.discount_price) * float(self.amount), 2) # total sin iva = precio unitario descontado * cantidad

        iva_percent = float(Business.objects.first().iva_percent) # porcentaje iva = 0.12

        if self.product.iva == "SI":
            self.discount_rate_12 = round(self.value_discount * float(self.amount),2) # descuento tarifa 12 = descuento unitario * cantidad
            self.discount_rate_00 = round(0.0,2) # descuento tarifa 00 = 0
            self.sale_rate_12 = round(self.total,2)# venta tarifa 12 = total sin iva
            self.sale_rate_00 = round(0.0,2) # venta tarifa 00 = 0
            self.iva_value = round(self.total * iva_percent,2) # iva valor = total sin iva * porcentaje iva
        else:
            self.discount_rate_12 = round(0.0,2) #descuento tarifa 12 = 0
            self.discount_rate_00 = round(self.value_discount * float(self.amount),2) # descuento tarifa 00 = descuento unitario * cantidad
            self.sale_rate_12 = round(0.0,2) # venta tarifa 12 = 0
            self.sale_rate_00 = round(self.total,2) # venta tarifa 00 = total sin iva
            self.iva_value = round(0.0,2) # iva valor = 0
        
        super(OrderDetail, self).save(*args, **kwargs) # Call the real save() method
    def get_iva_value(self):
        iva_percent = Business.objects.first().iva_percent
        return float(self.total * iva_percent)

    def get_discount_percent(self,count=None):
        percent = 0.0
        try:
            params = self.product_discount.first()
            if not count:
                count = self.amount
            if count > 0:
                if count >= params.min_amount_1 and count <= params.max_amount_1:
                    percent = params.percent_1
                elif count >= params.min_amount_2 and count <= params.max_amount_1:
                    percent = params.percent_2
                elif count >= params.min_amount_3 and count <= params.max_amount_1:
                    percent = params.percent_3
                elif count >= params.min_amount_4 and count <= params.max_amount_1:
                    percent = params.percent_4
                elif count >= params.min_amount_5 and count <= params.max_amount_1:
                    percent = params.percent_5
                elif count >= params.min_amount_6 and count <= params.max_amount_6:
                    percent = params.percent_6
        except:
            pass
        return percent

    class Meta:
        db_table = "tblfac_notapedidodetalle"

class ProductDiscount(models.Model):
    id = models.AutoField(db_column="Registro",primary_key=True)
    product = models.ForeignKey(Product, db_column="Producto", related_name="product_discount", on_delete=models.CASCADE)
    user = models.CharField(db_column='Usuario', max_length=45)
    created_at = models.DateTimeField(db_column="Creacion", auto_now_add=True)

    min_amount_1= models.IntegerField(db_column="CantidadMinima1",default=1)
    max_amount_1= models.IntegerField(db_column="CantidadMaxima1",default=30)
    percent_1 = models.FloatField(db_column="Porcentaje1",default=33.33)

    min_amount_2= models.IntegerField(db_column="CantidadMinima2",default=31)
    max_amount_2= models.IntegerField(db_column="CantidadMaxima2",default=120)
    percent_2 = models.FloatField(db_column="Porcentaje2",default=36.5)

    min_amount_3= models.IntegerField(db_column="CantidadMinima3",default=121)
    max_amount_3= models.IntegerField(db_column="CantidadMaxima3",default=420)
    percent_3 = models.FloatField(db_column="Porcentaje3",default=39.53)

    min_amount_4= models.IntegerField(db_column="CantidadMinima4",default=421)
    max_amount_4= models.IntegerField(db_column="CantidadMaxima4",default=750)
    percent_4 = models.FloatField(db_column="Porcentaje4",default=42.41)

    min_amount_5= models.IntegerField(db_column="CantidadMinima5",default=0) # unused
    max_amount_5= models.IntegerField(db_column="CantidadMaxima5",default=0) # unused
    percent_5 = models.FloatField(db_column="Porcentaje5",default=0) # unused

    min_amount_6= models.IntegerField(db_column="CantidadMinima6",default=0) # unused
    max_amount_6= models.IntegerField(db_column="CantidadMaxima6",default=0) # unused
    percent_6 = models.FloatField(db_column="Porcentaje6",default=0) # unused
    class Meta:
        db_table = "tblfac_productodescuento"


class Business(models.Model):
    id = models.AutoField(db_column="Empresa",primary_key=True)
    iva_percent = models.FloatField(db_column="PorcentajeIva",default=0.0)

    class Meta:
        db_table = "tblempresa"
