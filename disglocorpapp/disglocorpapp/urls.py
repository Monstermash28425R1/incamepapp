from django.contrib import admin
from django.urls import path, include
from disglocorpapp import test
from users import views
from orders.views import test_email, get_pdf,send_pdf

urlpatterns = [
    path("get/pdf/<order_id>",get_pdf),
    path("send/report/<order_id>",send_pdf),
    path("", include('home.urls')),
    path("pedidos/", include('orders.urls')),
    path('auth/', include('django.contrib.auth.urls')),
    #path('login', users.views, name='login')
    path('logout/', views.logout, name='logout')
]
