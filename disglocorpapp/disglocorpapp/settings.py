
import os
import environ
#

#

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

env = environ.Env(
    DEBUG=(bool,False)
)
environ.Env.read_env(env_file=BASE_DIR+"/.env")
# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'c3nop_k=klp!=pm+4pq3!lnvoppgxia%(pg3+6%6)3y=()!%_6'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env("DEBUG")

ALLOWED_HOSTS =["*"] #"localhost","127.0.0.1", "186.5.36.109"

#AUTH_USER_MODEL = 'users.User'
LOGIN_REDIRECT_URL = '/pedidos/registrar'
AUTHENTICATION_BACKENDS = [
    #'django.contrib.auth.backends.ModelBackend',
    'users.backends.AuthBackend',
]

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'bootstrap_pagination',
    #'nolastlogin',
    'django.contrib.humanize',
    'users',
    'home',
    'orders',
    'clients',
    'sellers',

]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'disglocorpapp.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR+"/templates"],
        'APP_DIRS': True,
        
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                
            ],
        },
    },
]

WSGI_APPLICATION = 'disglocorpapp.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': env("DB_NAME"),
        'USER': env("DB_USER"),
        'PASSWORD': env("DB_SECRET"),
        'HOST': env("DB_HOST",default="127.0.0.1"),
        'PORT': env("DB_PORT",default=3306),
    }
}


# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'es-es'

TIME_ZONE = 'America/Guayaquil'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_DIRS = [
    BASE_DIR +"/static"
]

# django-no-last-login
NO_UPDATE_LAST_LOGIN = True


EMAIL_HOST='p3plzcpnl445406.prod.phx3.secureserver.net' #'smtp.office365.com'
EMAIL_PORT=465 #587
EMAIL_HOST_USER='ventas@disglocorp.com'
EMAIL_HOST_PASSWORD= "Disglo0341"
SELLER_EMAIL="info@incamep.com"
EMAIL_USE_SSL=True
EMAIL_USE_TLS=False