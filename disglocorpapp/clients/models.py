from django.db import models

# TODO Address
# TODO country - Pais
# TODO state - Provincia
# TODO canton ?? - Canton
# TODO parish - parroquia
# TODO zone - zona
# TODO neighborhood - barrio


class Country(models.Model):
    id = models.AutoField(db_column="Registro",primary_key=True)
    name = models.CharField(db_column="Nombre", max_length=45)
    shortname = models.CharField(db_column="Abreviatura", max_length=45)
    code = models.IntegerField(db_column="Codigo",default=1)

    class Meta:
        db_table = 'tblpais'

class Province(models.Model):
    id = models.AutoField(db_column="Registro",primary_key=True)
    country = models.ForeignKey(Country,db_column="Pais", on_delete=models.CASCADE)
    name = models.CharField(db_column="Nombre", max_length=45)
    code = models.CharField(db_column="Codigo", max_length=45)

    class Meta:
        db_table = 'tblprovincia'

class Zone(models.Model):
    id = models.AutoField(db_column="Registro",primary_key=True)
    name = models.CharField(db_column="Nombre", max_length=45)
    shortname = models.CharField(db_column="Abreviatura", max_length=45)

    class Meta:
        db_table = 'tblzona'

class Canton(models.Model):
    id = models.AutoField(db_column="Registro",primary_key=True)
    country = models.ForeignKey(Country, db_column="Pais", on_delete=models.CASCADE)
    province = models.ForeignKey(Province, db_column="Provincia", on_delete=models.CASCADE)
    name = models.CharField(db_column="Nombre", max_length=45)
    code = models.CharField(db_column="Codigo", max_length=45)
    def __str__(self):
        return self.name
    class Meta:
        db_table = 'tblcanton'


class Parish(models.Model):
    id = models.AutoField(db_column="Registro",primary_key=True)

    country = models.ForeignKey(Country, db_column="Pais", on_delete=models.CASCADE)
    province = models.ForeignKey(Province, db_column="Provincia", on_delete=models.CASCADE)
    city = models.ForeignKey(Canton, db_column="Canton", on_delete=models.CASCADE)
    name = models.CharField(db_column="Nombre", max_length=45)
    code = models.CharField(db_column="Codigo", max_length=45)
    class Meta:
        db_table = 'tblparroquia'


class Client(models.Model):
    id = models.IntegerField(db_column='Codigo', primary_key=True)
    identification_type = models.CharField(db_column='TipoIdentificacion', max_length=45) # RUC, CEDULA
    identification = models.CharField(db_column='Identificacion', max_length=45)
    social_reason = models.CharField(db_column='RazonSocial', max_length=90)
    name = models.CharField(db_column='NombreComercial', max_length=90)
    comercial_activity = models.CharField(db_column='ActividadComercial', max_length=45)

    # Address
    country = models.ForeignKey(Country, db_column="Pais", on_delete=models.CASCADE)
    province = models.ForeignKey(Province, db_column="Provincia", on_delete=models.CASCADE)
    city = models.ForeignKey(Canton, db_column="Canton", related_name="clients", on_delete=models.CASCADE)
    parish = models.ForeignKey(Parish, db_column="Parroquia", on_delete=models.CASCADE)
    zone = models.ForeignKey(Zone, db_column="Zona", on_delete=models.CASCADE)
    # neighborhood - barrio
    street = models.CharField(db_column='Calle', max_length=45)
    street_number = models.CharField(db_column='Numero', max_length=45)
    interception = models.CharField(db_column='Interseccion', max_length=45)
    reference = models.CharField(db_column='Referencia', max_length=90)
    
    # Contact data
    phone = models.CharField(db_column='Telefono', max_length=45)
    mobile = models.CharField(db_column='Celular', max_length=45)
    fax = models.CharField(db_column='Fax', max_length=45)
    mail = models.CharField(db_column='Mail', max_length=90)
    website = models.CharField(db_column='Web', max_length=45)
    
    # Contacts data
    contact_1 = models.CharField(db_column='Contacto1', max_length=45)
    phone_contact_1 = models.CharField(db_column='TelefonoContacto1', max_length=45)
    contact_2 = models.CharField(db_column='Contacto2', max_length=90)
    phone_contact_2 = models.CharField(db_column='TelefonoContacto2', max_length=45)

    # Mercantil Data
    credit_limit = models.FloatField(db_column='CupoCredito')
    credit_days = models.IntegerField(db_column='DiasCredito')
    iva = models.CharField(db_column='Iva', max_length=45)

    def __str__(self) -> str:
        return "ID: " + str(self.id) + ", Name: " +self.name

    @property
    def address(self)-> str:
        return f"{self.province.name}, {self.city.name}, {self.parish.name}, {self.zone.name}"
    @property
    def street_address(self)-> str:
        return f"{self.street}, {self.street_number}, {self.interception}"


    class Meta:
        db_table = "tblcxc_cliente"

