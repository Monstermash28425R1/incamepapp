from django.contrib.auth.base_user import BaseUserManager
from django.db import models
from django.contrib.auth.signals import user_logged_in
from django.contrib.auth.models import update_last_login
from django.contrib import auth
from django.contrib.auth.models import AbstractUser
from sellers.models import Seller
#from django.contrib.auth.models import User as AbstractUser

def _no_last_login():
    return True


class UserManager(BaseUserManager):
    def create_user(self, email, password=None):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_staffuser(self, email, password):
        """
        Creates and saves a staff user with the given email and password.
        """
        user = self.create_user(
            email,
            password=password,
        )
        user.staff = True
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        """
        Creates and saves a superuser with the given email and password.
        """
        user = self.create_user(
            email,
            password=password,
        )
        user.staff = True
        user.admin = True
        user.save(using=self._db)
        return user


# TODO completar relacion con UsuarioDetalle
class User(models.Model):
    USERNAME_FIELD = 'name'
    EMAIL_FIELD = 'name'
    REQUIRED_FIELDS = ['name', 'password'],
    is_anonymous = False
    is_authenticated = False
    seller_id = None
    is_active = True

    id = models.IntegerField(db_column='Registro', primary_key=True, auto_created=True)
    name = models.CharField(db_column='Nombre', max_length=45, unique=True)
    password = models.CharField(db_column='Clave', max_length=45)
    level = models.CharField(db_column='Nivel', max_length=45)
    status = models.CharField(db_column='Estatus', max_length=45)
    last_login = False

    def save(self, *args, **kwargs):
        fields = kwargs.pop('update_fields', [])
        if fields != ['last_login']:
            return super(User, self).save(*args, **kwargs)

    def get_full_name(self):
        return self.name

    def get_short_name(self):
        return self.name

    def __str__(self):
        return str(self.id)

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return True
    
    @property
    def seller(self):
        slr = Seller.objects.filter(username=self.name).first()
        if not slr:
            return None
        self.seller_id = slr.id
        return slr

    @property
    def is_admin(self):
        return self.level == "ADMINISTRADOR"

    @property
    def seller(self):
        slr = Seller.objects.filter(username=self.name).first()
        if not slr:
            return None
        self.seller_id = slr.id
        return slr

    class Meta:
        db_table = "tblusuario"




class CustomSession(models.Model):
    user = models.CharField(db_column="Usuario", max_length=45, primary_key=True, unique=True)
    device = models.CharField(db_column="Equipo", max_length=45)
    ip_address = models.CharField(db_column="DireccionIp",max_length=45)
    created_at = models.DateTimeField(db_column="Creacion", auto_now_add=True)
    enterprise = models.IntegerField(db_column="Empresa")

    class Meta:
        db_table="tbllogin"

# if _no_last_login():
#     auth.signals.user_logged_in.disconnect(auth.models.update_last_login, dispatch_uid='update_last_login')
    #user_logged_in.disconnect(receiver=update_last_login, dispatch_uid='update_last_login')
