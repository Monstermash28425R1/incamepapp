from django.shortcuts import render
from users.managers import CustomAuthManager
from django.shortcuts import redirect
from django.http import HttpResponse
def login(request):
    auth_manager = CustomAuthManager(request)
    if (auth_manager.is_authenticated()):
        return redirect('/')
    if request.method == 'GET':
        return render(request, 'app/auth/login.html')
    return HttpResponse("Método POST no soportado para la ruta /auth/login")

def logout(request):
    auth_manager = CustomAuthManager(request)
    if (auth_manager.is_authenticated() == False):
        return redirect('/auth/login')
    auth_manager.destroy()
    return redirect('/auth/login')