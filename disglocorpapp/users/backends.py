#from django.contrib.auth.backends import BaseBackend
from django.contrib.auth.backends import ModelBackend
import django.dispatch
from django.contrib.auth.signals import user_logged_in
from django.contrib.auth.models import update_last_login
from users.models import *
from sellers.models import *
from users.managers import CustomAuthManager
from datetime import datetime

class AuthBackend(ModelBackend):

    def authenticate(self, request, **kwargs):
        name = kwargs['username']
        password = kwargs['password']
        try:
            name = name.upper()
            user = User.objects.get(name=name)
            if user.password == password and user.status == 'ACTIVO':
                auth_manager = CustomAuthManager(request)
                auth_manager.authenticate(user)
                ip_address = self.get_client_ip(request)
                
                # Verify with sellers table
                try:
                    slr = Seller.objects.filter(username=name).first()

                    entry = datetime.strptime(slr.entry,"%Y-%m-%d") if slr.entry and slr.entry !="" else None
                    exitt = datetime.strptime(slr.exitt,"%Y-%m-%d")if slr.exitt and slr.exitt !="" else None
                    now = datetime.now()
                    
                    if not slr or (entry and entry > now) or (exitt and exitt < now):
                        #raise User.DoesNotExist("Usuario inválido o deshabilitado")
                        return None
                except seller.models.DoesNotExist:
                    #raise User.DoesNotExist("Usuario inválido o deshabilitado")
                    return None

                ssn = CustomSession.objects.filter(user=user.name)
                if not ssn:
                    ssn = CustomSession.objects.create(user=user.name, device="REMOTO", ip_address=ip_address,created_at=datetime.now(), enterprise=1).save()
                else:
                    ssn = CustomSession.objects.filter(user=user.name).update(user=user.name, device="REMOTO", ip_address=ip_address,created_at=datetime.now(), enterprise=1)

                user.seller_id = slr.id
                #user_logged_in.disconnect(update_last_login)
                user_logged_in.disconnect(receiver=update_last_login)
                #django.dispatch.Signal.disconnect(receiver='update_last_login', sender='user_logged_in', dispatch_uid=None)
                return user
            else:
                raise User.DoesNotExist("Usuario inválido o deshabilitado")
                return None
        except User.DoesNotExist:
            return None
    def get_user(self, user_id):
        try:
            User.get(pk = user_id)
        except User.DoesNotExist:
            return None

    def get_client_ip(self,request):
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')
        return ip