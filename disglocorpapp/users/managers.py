from users.models import User

class CustomAuthManager(object):
    def __init__(self, request):
        self.request = request

    def authenticate(self, user):
        self.request.session['auth_user'] = user.id

    def is_authenticated(self):
        if self.user() != None and isinstance(self.user(), User):
            return True
        return False

    def destroy(self):
        self.request.session['auth_user'] = None
    def user(self):
        try:
            session_user = User.objects.get(id=self.request.session['auth_user'])
        except (KeyError, User.DoesNotExist):
            session_user = None
        return session_user